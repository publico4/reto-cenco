import React from "react";

import FilterDate from "./FilterDate";
import FilterFullName from "./FilterFullName";
import ClearFilter from "./ClearFilter";

const Filtros = () => {
  return (
    <div className="filterContainer">
      <FilterFullName />
      <FilterDate />
      <ClearFilter />
    </div>
  );
};

export default Filtros;
