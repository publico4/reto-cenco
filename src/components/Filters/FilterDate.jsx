import React, { useContext } from "react";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

import UserContext from "../../context/users/UserContext";

const FilterDate = () => {
  const { dateRange, filterByDate } = useContext(UserContext);
  const [startDate, endDate] = dateRange;

  return (
    <section className="filterRange">
      <label className="filterRange__text">Filtrar por fechas</label>
      <DatePicker
        selectsRange={true}
        dateFormat="dd/MM/yyyy"
        startDate={startDate}
        endDate={endDate}
        onChange={filterByDate}
        isClearable={false}
        placeholderText="Indique el rango de fechas a filtrar"
        className="filterRange__date"
      />
    </section>
  );
};

export default FilterDate;
