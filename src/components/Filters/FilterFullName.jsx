import React, { useState, useContext, useEffect } from "react";
import Select from "react-select";
import UserContext from "../../context/users/UserContext";

const FilterFullName = () => {
  const [selectedOption, setSelectedOption] = useState(null);
  const { usersSerch, filterByFullName, isClear } = useContext(UserContext);


  useEffect(() => {
    setSelectedOption(() => null);
  }, [isClear]);

  const handleChange = (value) => {
    setSelectedOption(value);
    filterByFullName(value);
  };

  return (
    <section className="filterText">
      <label className="filterText__label">Buscador</label>
      <Select
        defaultValue={selectedOption}
        value={selectedOption}
        onChange={handleChange}
        options={usersSerch}
        placeholder="Nombres o Apellidos"
        noOptionsMessage={() => "Campo no encontrado"}
        className="filterText__Select"
      />
    </section>
  );
};

export default FilterFullName;
