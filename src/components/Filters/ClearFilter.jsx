import React, { useContext } from "react";
import UserContext from "../../context/users/UserContext";

const ClearFilter = () => {
  const { clearAll } = useContext(UserContext);
  return (
    <div className="clearFilter">
      <button className="clearFilter__btn" onClick={clearAll}>
        Limpiar Filtros
      </button>
    </div>
  );
};

export default ClearFilter;
