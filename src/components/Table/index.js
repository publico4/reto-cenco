import React, { useContext } from "react";
import UserContext from "../../context/users/UserContext";

import DataRow from "./DataRow";
const Table = () => {
  const { usersTable, totalUsers, nextOrPrevPage } = useContext(UserContext);
  return (
    <>
      <table className="table">
        <thead className="table__header">
          <tr>
            <th className="table__header--info">Nombres</th>
            <th className="table__header--info">Apellidos</th>
            <th className="table__header--info">Edad</th>
            <th className="table__header--info">Nacionalidad</th>
            <th className="table__header--info">Fecha</th>
          </tr>
        </thead>
        <tbody className="table__body">
          {usersTable &&
            usersTable.map((item, index) => (
              <DataRow key={item.id} info={item} />
            ))}
        </tbody>
      </table>
      {totalUsers && totalUsers.length > 20 && (
        <section className="pagination">
          <button
            className="pagination__btn"
            onClick={() => nextOrPrevPage(-1)}
          >
            Atrás
          </button>
          <button
            className="pagination__btn"
            onClick={() => nextOrPrevPage(+1)}
          >
            Siguiente
          </button>
        </section>
      )}
    </>
  );
};

export default Table;
