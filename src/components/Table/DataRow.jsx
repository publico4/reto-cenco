import React from "react";

const DataRow = ({ info }) => {
  const { name, lastName, age, nationality, date } = info;
  return (
    <tr>

      <td className="table__body--data" data-label="Nombres">
        {name}
      </td>
      <td className="table__body--data" data-label="Apellidos">
        {lastName}
      </td>
      <td className="table__body--data" data-label="Edad">
        {age}
      </td>
      <td className="table__body--data" data-label="Nacionalidad">
        {nationality}
      </td>
      <td className="table__body--data" data-label="Fecha">
        {date}
      </td>
    </tr>
  );
};

export default DataRow;
