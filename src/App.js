import { useContext, useEffect } from "react";
import UserContext from "./context/users/UserContext";
import Filtros from "./components/Filters";
import Table from "./components/Table";

function App() {
  const { getUsers } = useContext(UserContext);

  useEffect(() => {
    getUsers();
    // eslint-disable-next-line
  }, []);

  return (
    <div className="App">
      <Filtros />
      <Table />
    </div>
  );
}

export default App;
