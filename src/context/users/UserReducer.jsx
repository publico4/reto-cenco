import moment from "moment/moment";
const reducer = (state, action) => {
  switch (action.type) {
    case "SUCCESS_USERS": {
      let dataUsers = [...action.payload];
      let filterForTable = dataUsers.filter(
        (item, index) => index < state.rowsPerPage
      );
      return {
        ...state,
        totalUsers: dataUsers,
        UserForDateRange: dataUsers,
        usersTable: filterForTable,
        usersSerch: formatDataSearch([...filterForTable]),
        totalPages: Math.ceil(dataUsers.length / state.rowsPerPage),
      };
    }
    case "FILTER_BY_FULLNAME": {
      const usersFilterFullName = [...state.totalUsers];
      return {
        ...state,
        dateRange: [null, null],
        usersTable: usersFilterFullName.filter(
          (item) => item.id === action.payload
        ),
      };
    }
    case "NEXT_OR_PREV_PAGE": {
      let operation = state.page + action.payload;
      let review;
      if (action.payload > 0) {
        review = operation < state.totalPages ? operation : state.totalPages;
      } else {
        review = operation > 1 ? operation : 1;
      }
      return {
        ...state,
        usersTable: sliceData([...state.totalUsers], review, state.rowsPerPage),
        page: review,
      };
    }

    case "UPDATE_DATE_RANGE": {
      return {
        ...state,
        dateRange: action.payload,
      };
    }

    case "FILTER_BY_DATE_RANGE": {
      const UsersForFilterDate = [...state.totalUsers];
      const startDate = moment(action.payload.startDate);
      const endDate = moment(action.payload.endDate);
      const temporalFilterRange = filterByRange(
        UsersForFilterDate,
        startDate,
        endDate
      );
      return {
        ...state,
        UserForDateRange: temporalFilterRange,
        usersTable: sliceData([...temporalFilterRange], 1, state.rowsPerPage),
        hasFilterDate: true,
        isClear: !state.isClear,
      };
    }

    case "CLEAR_ALL": {
      const allDataUserClear = [...state.totalUsers];
      return {
        ...state,
        dateRange: [null, null],
        isClear: !state.isClear,
        usersTable: allDataUserClear.filter((item, index) => index < 20),
      };
    }

    default:
      return state;
  }
};

const formatDataSearch = (data) => {
  return data.map((item) => ({
    value: item.id,
    label: `${item.name} ${item.lastName}`,
  }));
};

const filterByRange = (data, startDate, endDate) => {
  return data.filter((item) => {
    let date = moment(item.date, ["DD/MM/YYYY", "MM/DD/YYYY"]);
    return date.diff(startDate, "days") >= 0 && date.diff(endDate, "days") <= 0;
  });
};

const sliceData = (data, page, rowsPerPage) => {
  return data.slice((page - 1) * rowsPerPage, page * rowsPerPage);
};

export default reducer;
