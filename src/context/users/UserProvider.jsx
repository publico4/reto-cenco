import React, { useReducer } from "react";
import UserReducer from "./UserReducer";
import UserContext from "./UserContext";

const UserProvider = ({ children }) => {
  const initialState = {
    totalUsers: null,
    UserForDateRange: null,
    usersTable: null,
    usersSerch: null,
    page: 1,
    rowsPerPage: 20,
    totalPages: 0,
    dateRange: [null, null],
    hasFilterDate: false,
    isClear: false,
  };

  const getUsers = async () => {
    const result = await fetch("http://localhost:3000/data.json");
    const { data } = await result.json();
    dispatch({
      type: "SUCCESS_USERS",
      payload: data,
    });
  };

  const filterByFullName = (info) => {
    dispatch({
      type: "FILTER_BY_FULLNAME",
      payload: info.value,
    });
  };

  const filterByDate = (rangeUpdate) => {
    const [startDate, endDate] = rangeUpdate;

    dispatch({
      type: "UPDATE_DATE_RANGE",
      payload: rangeUpdate,
    });

    if (!!startDate && !!endDate) {
      dispatch({
        type: "FILTER_BY_DATE_RANGE",
        payload: { startDate, endDate },
      });
    }
  };

  const nextOrPrevPage = (number) => {
    dispatch({
      type: "NEXT_OR_PREV_PAGE",
      payload: number,
    });
  };

  const clearAll = () => {
    dispatch({
      type: "CLEAR_ALL",
    });
  };

  const [state, dispatch] = useReducer(UserReducer, initialState);
  return (
    <UserContext.Provider
      value={{
        getUsers,
        filterByFullName,
        clearAll,
        nextOrPrevPage,
        filterByDate,
        totalUsers: state.totalUsers,
        usersTable: state.usersTable,
        usersSerch: state.usersSerch,
        totalPages: state.totalPages,
        dateRange: state.dateRange,
        isClear: state.isClear,
      }}
    >
      {children}
    </UserContext.Provider>
  );
};

export default UserProvider;
